########################
# SHOULD BE RUN AS ROOT
########################
from nest.topology import *

# This program emulates a point to point network between two hosts `h1` and
# `h2`. Five ping packets are sent from `h1` to `h2`, and the success/failure
# of these packets is reported.

#################################
#       Network Topology        #
#                               #
#          5mbit, 5ms -->       #
#   h1 ------------------- h2   #
#       <-- 10mbit, 100ms       #
#                               #
#################################

h1 = Node("h1")
h2 = Node("h2")

(eth1, eth2) = connect(h1, h2)

# Note: it is important to mention the subnet.
eth1.set_address("192.168.1.1/24")
eth2.set_address("192.168.1.2/24")

eth1.set_attributes("5mbit", "5ms")
eth2.set_attributes("10mbit", "100ms")

# `Ping` from `h1` to `h2`.
h1.ping(eth2.address)
h2.ping(eth1.address)

